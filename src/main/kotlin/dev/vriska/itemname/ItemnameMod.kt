// This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.

package dev.vriska.itemname

import net.fabricmc.api.ModInitializer
import net.fabricmc.fabric.api.command.v1.CommandRegistrationCallback
import net.minecraft.server.command.CommandManager
import net.minecraft.server.command.CommandManager.literal
import net.minecraft.server.command.CommandManager.argument
import net.minecraft.server.command.ServerCommandSource
import net.minecraft.text.LiteralText
import com.mojang.brigadier.context.CommandContext
import com.mojang.brigadier.arguments.StringArgumentType.getString
import com.mojang.brigadier.arguments.StringArgumentType.greedyString
import com.mojang.brigadier.exceptions.SimpleCommandExceptionType
import eu.pb4.placeholders.TextParser

class ItemnameMod : ModInitializer {
    override fun onInitialize() {
        CommandRegistrationCallback.EVENT.register {
            dispatcher, _ ->
            dispatcher.register(
                literal("itemname")
                    .then(
                        argument("name", greedyString())
                            .executes({ ctx -> execute(ctx.getSource(), getString(ctx, "name")) }))
                    .executes({ ctx -> execute(ctx.getSource(), null) }))
        }
    }

    private fun execute(source: ServerCommandSource, name: String?): Int {
        val self = source.getPlayer()
        val inventory = self.getInventory()
        val hand = inventory.getMainHandStack()

        if (hand.isEmpty()) {
            throw SimpleCommandExceptionType(LiteralText("No item selected!")).create()
        }

        val renamed = if (name != null) {
            val text = TextParser.parseSafe(name)
            hand.setCustomName(text)
        } else {
            hand.removeCustomName()
            hand
        }

        inventory.setStack(inventory.selectedSlot, renamed)

        return 0
    }
}
