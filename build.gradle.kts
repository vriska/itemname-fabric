// This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.

import com.modrinth.minotaur.TaskModrinthUpload

plugins {
    kotlin("jvm") version "1.6.10"
    id("fabric-loom") version "0.10-SNAPSHOT"
    id("com.modrinth.minotaur") version "1.2.1"
    `maven-publish`
    java
}

group = property("maven_group")!!
version = property("mod_version")!!

repositories {
    maven {
        url = uri("https://maven.nucleoid.xyz/")
    }
}

dependencies {
    minecraft("com.mojang:minecraft:${property("minecraft_version")}")
    mappings("net.fabricmc:yarn:${property("yarn_mappings")}:v2")
    modImplementation("net.fabricmc:fabric-loader:${property("loader_version")}")

    modImplementation("net.fabricmc:fabric-language-kotlin:${property("fabric_kotlin_version")}")
    modImplementation("net.fabricmc.fabric-api:fabric-api:${property("fabric_version")}")

    modImplementation(include("eu.pb4:placeholder-api:1.1.3+1.17.1")!!)
}

tasks {
    processResources {
        inputs.property("version", project.version)
        filesMatching("fabric.mod.json") {
            expand("version" to project.version)
        }
    }

    jar {
        from("LICENSE")
    }

    compileKotlin {
        kotlinOptions.jvmTarget = "17"
    }
}

tasks.register<TaskModrinthUpload>("publishModrinth") {
    onlyIf {
        System.getenv("MODRINTH") != null
    }

    dependsOn("remapJar")

    token = System.getenv("MODRINTH")
    projectId = "bzsIzJ6j"
    versionNumber = project.version as String
    uploadFile = tasks.remapJar.get()
    addGameVersion(project.property("minecraft_version") as String)
    addLoader("fabric")
}

publishing {
    publications {
        create<MavenPublication>("itemname") {
            from(components["java"])
        }
    }

    repositories {

    }
}

java {
    // Loom will automatically attach sourcesJar to a RemapSourcesJar task and to the "build" task
    // if it is present.
    // If you remove this line, sources will not be generated.
    withSourcesJar()
}
